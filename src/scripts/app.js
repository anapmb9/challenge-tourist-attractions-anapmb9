import { TouristAttractions } from "./components/TouristAttractions.js";

document.addEventListener("DOMContentLoaded", () => {
    new TouristAttractions();
});
