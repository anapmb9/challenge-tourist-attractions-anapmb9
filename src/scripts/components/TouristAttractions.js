export class TouristAttractions {
    constructor() {
        this.spot = JSON.parse(localStorage.getItem("spot")) || [
            {
                description:
                    "Amet minim mollit non deserunt ullamco est sit aliqua dolor dosa amet sint. Velit officia consece duis enim velit mollit.",
                image: "/images/pao-de-acucar.png",
                title: "Pão de Açúcar",
            },
            {
                description:
                    "Amet minim mollit non deserunt ullamco est sit aliqua dolor dosa amet sint. Velit officia consece duis enim velit mollit.",
                image: "/images/ilha-grande.png",
                title: "Ilha Grande",
            },
            {
                description:
                    "Amet minim mollit non deserunt ullamco est sit aliqua dolor dosa amet sint. Velit officia consece duis enim velit mollit.",
                image: "/images/cristo-redentor.png",
                title: "Cristo Redentor",
            },
            {
                description:
                    "Amet minim mollit non deserunt ullamco est sit aliqua dolor dosa amet sint. Velit officia consece duis enim velit mollit.",
                image: "/images/centro-paraty",
                title: "Centro Histórico de Paraty",
            },
        ];

        this.selectors();
        this.events();
    }

    selectors() {
        this.form = document.querySelector(".tourist-attractions-form");
        this.inputImage = document.querySelector(".tourist-attractions-input-image");
        this.imageWrapper = document.querySelector(".tourist-attractions-image-wrapper");
        this.textareaDescription = document.querySelector(
            ".tourist-attractions-textarea-description"
        );
        this.inputTitle = document.querySelector(".tourist-attractions-input-title");
        this.resultsContainer = document.querySelector(".tourist-attractions-results-container");
    }

    events() {
        window.addEventListener("load", this.renderItems(this.spot));
        this.inputImage.addEventListener("change", this.pictureFrame.bind(this));
        this.form.addEventListener("submit", (e) => {
            this.addTouristAttractions(e);
        });
    }

    pictureFrame() {
        const image = this.inputImage.files[0];

        const fileReader = new FileReader();
        fileReader.readAsDataURL(image);
        fileReader.onload = () => {
            const imagePreview = document.createElement("img");
            imagePreview.setAttribute("src", fileReader.result);
            imagePreview.classList.add("tourist-attractions-preview");
            const deleteButton = document.createElement("div");
            deleteButton.classList.add("tourist-attractions-del-img-button");
            this.imageWrapper.append(imagePreview, deleteButton);

            deleteButton.addEventListener("click", this.delImage.bind(this));
        };
    }

    renderItems(arr) {
        arr.forEach((elem) => {
            const attractions = this.createItem(elem);

            this.resultsContainer.appendChild(attractions);
        });
    }

    createItem(elem) {
        const attractions = document.createElement("div");
        attractions.classList.add("tourist-attractions-card");

        attractions.innerHTML = `
            <img
            src=${elem.image}
            alt="tourist-attractions-image"
            class="tourist-attractions-card-image"
          />
          <div class="tourist-attractions-card-info">
            <h3 class="tourist-attractions-card-title">${elem.name}</h3>

            <p class="tourist-attractions-card-description">
              ${elem.description}
            </p>
          </div>
        `;
        return attractions;
    }

    addItem(e) {
        e.preventDefault();
        const image = this.inputImage.files[0];
        const title = this.inputTitle.value;
        const description = this.textareaDescription.value;

        const fileReader = new FileReader();
        fileReader.readAsDataURL(image);

        fileReader.onload = () => {
            const item = {
                image: fileReader.result,
                title: title,
                description: description,
            };
            this.spot.push(item);

            const itemDOM = this.createItem(this.spot[this.spot.length - 1]);
            this.resultsContainer.appendChild(itemDOM);

            this.reset();

            localStorage.setItem("spot", JSON.stringify(this.spot));
        };
    }

    reset() {
        this.inputTitle.value = "";
        this.textareaDescription.value = "";

        this.delImagePreview();
    }

    delImagePreview() {
        const imagePreview = document.querySelector(".tourist-attractions-preview");
        const delButton = document.querySelector(".tourist-attractions-del-img-button");
        this.inputImage.value = "";
        this.imageWrapper.removeChild(imagePreview);
        this.imageWrapper.removeChild(delButton);
    }
}
